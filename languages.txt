ast   "Asturian" "Asturianu" 74%
de    "German" "Deutsch" 99%
de_CH "German (Switzerland)" "Deutsch (Schweiz)" 99%
en    "English" "English" 100%
en_AU "English (Australia)" "English (Australia)" 70%
es    "Spanish" "Español" 99%
fr    "French" "Français" 100%
ga    "Irish" "Irish" 30%
it    "Italian" "Italiano" 100%
hu    "Hungarian" "Magyar" 44%
nl    "Dutch" "Nederlands" 63%
pl    "Polish" "Polski" 81%
pt    "Portuguese" "Português" 78%
pt_BR "Portuguese (Brazil)" "Português (Brasil)" 100%
ro    "Romanian" "Romana" 68%
fi    "Finnish" "Suomi" 99%
sv    "Swedish" "Svenska" 99%
tr    "Turkish" "Türkçe" 51%
cs    "Czech" "Čeština" 31%
el    "Greek" "Ελληνική" 44%
be    "Belarusian" "Беларуская" 50%
bg    "Bulgarian" "Български" 66%
ru    "Russian" "Русский" 100%
sr    "Serbian" "Српски" 59%
uk    "Ukrainian" "Українська" 50%
zh_TW "Chinese (Taiwan)" "中文（正體字）" 100%
zh_CN "Chinese (China)" "中文（简体字）" 100%
zh_HK "Chinese (Hong Kong)" "中文（香港字）" 100%
ja_JP "Japanese" "日本語" 99%
ko    "Korean" "한국의" 35%
